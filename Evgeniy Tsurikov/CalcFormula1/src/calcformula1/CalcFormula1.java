/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calcformula1;
import java.util.Scanner;

/**
 *
 * @author Евгений
 */
public class CalcFormula1 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        
        double x1,x2,y1,y2,d;
        System.out.println("Enter x1, x2, y1, y2: ");
        x1=in.nextDouble();
        x2=in.nextDouble();
        y1=in.nextDouble();
        y2=in.nextDouble();
        d=Math.sqrt(Math.pow(x1-x2, 2)+Math.pow(y2-y1, 2));
        System.out.print("Result d: ");
        System.out.printf("%.2f\n", d);   
    }
    
}
