/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calcformula3;

import java.util.Scanner;

/**
 *
 * @author Евгений
 */
public class CalcFormula3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double s,r;
        Scanner in = new Scanner(System.in);
        System.out.println("Insert r: ");
        r=in.nextDouble();
        s=Math.PI*Math.pow(r, 2);
        System.out.printf("%.2f\n", s);
    }
    
}
