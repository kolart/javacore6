/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calcformula10;

import java.util.Scanner;

/**
 *
 * @author Евгений
 */
public class CalcFormula10 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double s, a, b, c, p;
        Scanner in = new Scanner(System.in);
        System.out.println("Insert A B C: ");
        a=in.nextDouble();
        b=in.nextDouble();
        c=in.nextDouble();
        p=(a+b+c)/2;
        s=Math.sqrt(p*(p-a)*(p-b)*(p-c));
        System.out.printf("%.2f\n", s);
    }
    
}
