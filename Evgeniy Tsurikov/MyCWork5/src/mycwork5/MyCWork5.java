package mycwork5;

import java.util.Scanner;

public class MyCWork5 {

    public static void main(String[] args) {
        System.out.println("Задача 5");
        System.out.println("Введите число: ");
        Scanner in=new Scanner(System.in);
        String str=in.nextLine();
        int digit=Integer.parseInt(str);
        int digit1=digit/100;
        int digit2=(digit/10)%10;
        int digit3=digit%10;
        int finaldigit=digit1*100+digit3*10+digit2;
        System.out.println(finaldigit);
        if(finaldigit%2==0){
            System.out.println("Четное");
        }
        else
            System.out.println("Нечетное");
    }
    
}
