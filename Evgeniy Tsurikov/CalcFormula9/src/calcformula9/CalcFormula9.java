/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calcformula9;

import java.util.Scanner;

/**
 *
 * @author Евгений
 */
public class CalcFormula9 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double s, a, b, c, r;
        Scanner in = new Scanner(System.in);
        System.out.println("Insert A B C: ");
        a=in.nextDouble();
        b=in.nextDouble();
        c=in.nextDouble();
        System.out.println("Insert R: ");
        r=in.nextDouble();
        s=a*b*c/4/r;
        System.out.printf("%.2f\n", s);
    }
    
}
