package mycwork4;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
public class MyWindow extends JFrame implements ActionListener {
    private JLabel lblMessage;
    private JTextField txtInput1,txtInput2,txtInput3;   
    private JButton btnCalc;
    private JLabel lblResult;
    public MyWindow(){
        lblMessage=new JLabel("введите стороны a,b,c");
        txtInput1=new JTextField("input1");
        txtInput2=new JTextField("input2");
        txtInput3=new JTextField("input3");
        btnCalc=new JButton("Вычислить");
        lblResult=new JLabel("результат?");
        
        this.setSize(400,100);
        this.setTitle("Задача 4 ");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new FlowLayout());
        this.add(lblMessage);
        this.add(txtInput1);
        this.add(txtInput2);
        this.add(txtInput3);
        this.add(btnCalc);
        this.add(lblResult);
        btnCalc.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.setTitle("обработка действия");
        String txt1=txtInput1.getText();
        String txt2=txtInput2.getText();
        String txt3=txtInput3.getText();
        //this.setTitle("Вы ввели "+txtV); 
        int a= Integer.parseInt(txt1);
        int b= Integer.parseInt(txt2);
        int c= Integer.parseInt(txt3);
        if(a+b>c && a+c>b && b+c>a)
            lblResult.setText("треугольник существует");
        else
            lblResult.setText("треугольник не существует");
    }
}
