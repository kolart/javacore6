/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calcformula6;

import java.util.Scanner;

/**
 *
 * @author Евгений
 */
public class CalcFormula6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double s,d1,d2;
        Scanner in = new Scanner(System.in);
        System.out.println("Insert d1: ");
        d1=in.nextDouble();
        System.out.println("Insert d2: ");
        d2=in.nextDouble();
        s=d1*d2/2;
        System.out.printf("%.2f\n", s);
    }
    
}
