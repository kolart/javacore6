/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calcformula7;

import java.util.Scanner;

/**
 *
 * @author Евгений
 */
public class CalcFormula7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // correct formula divided by 4 not 2;
        double s, a;
        Scanner in = new Scanner(System.in);
        System.out.println("Insert a: ");
        a=in.nextDouble();
        s=Math.pow(a,2)*Math.sqrt(3)/4;
        System.out.printf("%.2f\n", s);
    }
    
}
