package mycwork3;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
public class MyWindow extends JFrame implements ActionListener {
    private JLabel lblMessage;
    private JTextField txtInput;
    private JButton btnCalc;
    private JLabel lblResult;
    public MyWindow(){
        lblMessage=new JLabel("введите");
        txtInput=new JTextField("0");
        btnCalc=new JButton("Вычислить");
        lblResult=new JLabel("результат?");
        
        this.setSize(400,100);
        this.setTitle("Задача 3 ");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new FlowLayout());
        this.add(lblMessage);
        this.add(txtInput);
        this.add(btnCalc);
        this.add(lblResult);
        btnCalc.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.setTitle("обработка действия");
        String txtV=txtInput.getText();
        int numb= Integer.parseInt(txtV);
        int sum=0;
        for(int i=1;i<=numb;i++){
            sum+=i+i*10;
        }
        lblResult.setText("результат = " +sum);  
    }
}
