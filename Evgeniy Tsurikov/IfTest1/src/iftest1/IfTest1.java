package iftest1;

import java.util.Scanner;

/**
 *
 * @author Евгений
 */
public class IfTest1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        double r=0;
        r=in.nextDouble();
        if(r<=0){
            System.out.println("радиус >0");
            return;
        }
        double length=2*Math.PI*r;
        if(length>5){
            System.out.println("утверждение длина больше 5 истинно");
        }
    }
    
}
