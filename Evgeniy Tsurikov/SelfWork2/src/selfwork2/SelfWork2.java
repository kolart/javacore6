/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package selfwork2;

import java.util.Scanner;

public class SelfWork2 {
    public static Double dbl;
    public static boolean pointExists=false;
    public static boolean isValid=true;
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.print("Input number: ");
        Scanner in=new Scanner(System.in);
        String s= in.nextLine();
        System.out.println(__check(s));
    }
    public static String __check(String s){
        int count=0;
        for(int i=0;i<s.length();i++){
            if(!Character.isDigit(s.charAt(i))){
                count++;
                if(s.charAt(i)=='.'){
                    pointExists=true;
                }
                else{
                    isValid=false;
                    break;
                }
            }
        } 
        if(isValid){
            if(pointExists){
                return "decimal number";
            }
            else{
                return "not decimal number";
            }
        }
        return "not number";
    }
}
