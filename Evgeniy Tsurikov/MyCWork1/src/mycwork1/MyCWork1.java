package mycwork1;

import java.util.Scanner;

public class MyCWork1 {


    public static void main(String[] args) {
        System.out.println("Задача 1");
        System.out.println("Введите число: ");
        Scanner in=new Scanner(System.in);
        String str=in.nextLine();
        int digit=Integer.parseInt(str);
        System.out.println(DigitCount(digit));
    }
    public static int DigitCount(int K){
        if(K<0){
            return DigitCount(-K);
        }
        else if(K>9){
            return 1+DigitCount(K/10);
        }
        else
            return 1;
    }
    
    
}
