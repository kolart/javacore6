/*
Начальный вклад в банке равен 1000 грн. В конце каждого месяца размер вклада
увеличивается на P процентов от имеющейся суммы (P — вещественное число, 0 < P < 25). По
данному P определить, через сколько месяцев размер вклада превысит 1100 грн., и вывести
найденное количество месяцев K (целое число) и итоговый размер вклада S (вещественное
число).
 */
package mycwork6;

import java.util.Scanner;

public class MyCWork6 {

    public static void main(String[] args) {
        int start=1000;
        System.out.println("Задача 6");
        System.out.println("Введите число: ");
        Scanner in=new Scanner(System.in);
        String str=in.nextLine();
        double P=Double.parseDouble(str);
        double percent=P*0.01;
        int count=0;
        for(;start<=1100;){
            start+=start*percent;
            count++;
        }
        System.out.println("Months: "+count+" End sum: "+start);
    }
    
}
