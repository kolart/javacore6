
package stringexample1;


public class StringExample1 {


    public static void main(String[] args) {
       //String
       //StringBuilder
       String str = "Привет, мир!";
       //            012345678901
       //строки это набор букв, есть возможность
       //обратиться к каждой букве
       //нумерация начинается от нуля
       //все функции, которые нужны для работы со
       //строками хранятся внутри строки
       //примеры:
       //1) вывести содержимое строки в обратном порядке
       //!рим ,тевирП
       //2) определить, сколько слов в строке
       //3) определить, есть ои в строке буква а

       //вывести на экран первую (последнюю)
       int index = 0;
       System.out.println(str.charAt(index));
       index = 11;
       System.out.println(str.charAt(index));
       int n = str.length();
       System.out.println("Длинна строки \""+str+ "\" равна "+n);
       //номер последней буквы = str.length() - 1;
       for(int i = str.length() - 1; i >= 0; i--)
       {
           //System.out.println(str.charAt(i));
           System.out.print(str.charAt(i)+"\n");
           //System.out.print(str.charAt(i)+'\n');
       }
        char k = 'а';//латинская буква
        //String k = "a";
        str = str+"Это я - Вася!";
                System.out.println(str);
         for(int i = 0; i <= str.length() - 1; i++)
         {
             char ch = str.charAt(i);
             if(ch == k)
             {
                 System.out.println("найдено");
             }
         }
          //String char StringBuilder
          String str1 = new String("hello");
          String str2 = new String("hello");
          //String str1 = "hello";
          //String str2 = "hello";
          if (str1 == str2)
          {
              System.out.println("строка "+str1+"' равна '"+str2+"'");
              //                1             2           4   
              //                        3              6          8
              //                                5 
              //                                            7
              //                                                9
          }else
          {
              System.out.println("строка "+str1+"' неравна '"+str2+"'");
          }
          //тип для работы с отдельным символом (разных кодировок)
          for(int i = 0; i <= 255; i++)
          {
              char CH = (char) i;
              if(i % 16 == 0)
              {
                  System.out.println(CH);
              }
              else
              System.out.print(CH);
          }
          //Hello, world! Это Я - Вася!
          //Количество слов = количество пробелов + 1
          //charAt(i) != ' ' && charAt(i + 1) == ' '
          //             012345678901234567890123456   - 26
          
            int counter = 0;
            for(int i = 0; i <= str.length() - 1; i++)
            {
                if (str.charAt(i) == ' ')
                {
                    counter++;
                }
            }
          System.out.println("строка = '"+str+"'");
          System.out.print("Количество слов = " + (counter + 1));
          System.out.println();
          String [] words = str.split(" ");
          int N = words.length;
           
    }

}
