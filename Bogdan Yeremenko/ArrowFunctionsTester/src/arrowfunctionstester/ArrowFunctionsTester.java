
package arrowfunctionstester;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class ArrowFunctionsTester {

   public static boolean isOdd(int x)
   {
       int m = x % 2;
       if (m == 0)
       {
           return true;
       }
       else
       {
           return false;
       }
   }
   public static boolean isEven(int x)
   {
       return x % 2 != 0;
   }

    private static void printIntegers(List<Integer> numbers, Predicate<Integer> even) {
    
    }
   void printIntegers(int[] numbers)
   {
       for(int i = 0; i < numbers.length; i++)
       {
           if (isOdd(numbers[i]))
           {
               System.out.print(numbers[i]+" ");
           }
       }
   }
   
   void printIntegers(int[] numbers, Predicate<Integer> funct)
   {
       for(int i = 0; i < numbers.size(); i++)
       {
           //if (isOdd(numbers[i]))
           if(funct.test(numbers.get(i)))
           {
               System.out.print(numbers.get(i)+" ");
           }
       }
   }
    public static void main(String[] args) {
       
        System.out.println("Hello from ArrowFunctionTester");
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        System.out.println("printIntegers (numbers, isEven)");
        Predicate<Integer> isEven = num -> (num % 2) == 0;
        printIntegers(numbers, isEven);
        System.out.println();
        printIntegers(numbers, num -> (num % 2) != 0);
        System.out.println();
        printIntegers(numbers, num -> true);
        
    }
    
}
