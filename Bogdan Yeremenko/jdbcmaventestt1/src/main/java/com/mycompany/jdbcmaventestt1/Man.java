
package com.mycompany.jdbcmaventestt1;
//правило создания
//правила отображения на єкране метод ToString()
//определение правил сравнения элементов
//правило задичения одинаковых элнментов
public class Man {

    //обязательно все поля из таблицы в базе данных
    @Override
    public String toString() {
        return "Man{" + "firstName=" + firstName + ", phone=" + phone + '}';
    }

    public String getFirstName() {
        return firstName;
    }

    private void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
   private String firstName;
   private String phone;
   private Man(){}
   public Man(String Fname, String phone)
   {
       this.firstName = Fname;
       this.phone = phone;
   }
}
