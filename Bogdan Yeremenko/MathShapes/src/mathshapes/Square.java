
package mathshapes;


public class Square implements IShape {

    public Square(double a) {
        this.a = a;
    }
   protected double a;

    @Override// для обжекта
    public String toString() {
        return "квадрат со стороной" + "a=" + a;
    }

    @Override//для интерфейса
    public double calcArea() {
        return a * a;
    }
   
}
