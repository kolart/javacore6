
package employeetester;


public class Acounter extends IEmployee {

    double salary;
    public Acounter(String title,double base)
    {
        super(title);
        this.salary = base;
    }
    
    @Override
    public double calcSalary() 
    {
        return salary;    
    }
    
}
