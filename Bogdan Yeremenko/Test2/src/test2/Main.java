
package test2;


import java.util.Arrays;
import java.util.Comparator;

public class Main {
  public static void main(String[] args) {
    String s = "Сегодня у меня хорошее настроение";
    String longest = Arrays.stream(s.split(" ")).max(Comparator.comparingInt(String::length)).orElse(null);
    System.out.println(longest);
    System.out.println("Количество символом в слове " + longest + " : 10");
  }
}
