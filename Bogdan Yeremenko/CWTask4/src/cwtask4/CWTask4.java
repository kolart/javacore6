
package cwtask4;


public class CWTask4 {
    
    
    private static int digitN(int number, int N)
    {
    
    int K = (int) Math.ceil(Math.log10(Math.abs(number) + 0.5));
    if( K > N )
        return -1;
    else
        return K;
    }

    
    public static void main(String[] args) {
        //три варианта решения. В зависимости от того, на чем делаем акцнт или фокус
        //условие: считаем количество цифр в том числе, которое ввел ользователь
        //вариант 1 - главное синтаксис функции
        //решение математическое
        int number = 324;
        int N = 4;
        int Count = digitN(number,4); //number мое число, 4 = мои ожидания (проверяю на четырех значность
        if(Count == N)
       System.out.println("Количество цифр цисла = " + Count);
       else
             System.out.println("Количество цифр в числе "+number+" отличается от "+N);
       // вариант 2 = самый известный = это через циклы и операцию деления = повторяем деление на 10 пока есть что делаить
       
       // вариант 3 = чисто рограммисткий = через строки
    }
    
}
