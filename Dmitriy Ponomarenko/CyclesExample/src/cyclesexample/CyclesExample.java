
package cyclesexample;

import java.util.Scanner;

public class CyclesExample {

   
    public static void main(String[] args) {
    //если введено отрицательное значение, то повторить ввод
    //while 25%
    //do-while 5% (Для конкретнх задач) = различные проверки ввода
    //for 70%
    Scanner in = new Scanner (System.in);
    int r = 0;
    do
    {
     System.out.println("Введите радиус. Число > 0");
    r = in.nextInt();
    
    } while(r<= 0);
    // защита от дурака
    double L = 2 * Math.PI * r;
    System.out.println("Длина окружности равна"+L);
    
    int number = 324;
    int counter = 0;
    int x = number;
    while ( x != 0)
    {
        x = x / 10;
        counter = counter + 1;
    }
    System.out.println("В числе" + number+"количество цифр="+counter);
     System.out.println("Все целые числа из первого десятка");
     for(int i = 1 ; i <= 10 ; i = i + 1)
     {
     System.out.println(i);
     }
    }
        
    }
    
}
