
package javaswingexample;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;



public class MyWindow extends JFrame implements ActionListener
{
    private JLabel lblMessage;
    private JTextField txtInput;
    private JButton btnCalc;
    private JLabel lblResult;
    
    public MyWindow()
    {
        lblMessage = new JLabel("Введите x");
        txtInput = new JTextField("Привет, мир");
        btnCalc = new JButton("Вычислить");
        lblResult =new JLabel("результать = ");
        
        this.setSize(400, 100);
        this.setTitle("Площадь квадрата");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        this.setLayout(new  FlowLayout());
        this.add(lblMessage);
        this.add(txtInput);
        this.add(btnCalc);
        this.add(lblResult);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        this.setTitle("обработка действия");
        String txtV = txtInput.getText();
        this.setTitle("Вы ввели"+txtV);
        
//To change body of generated methods, choose Tools | Templates.
    }
    
    
}
