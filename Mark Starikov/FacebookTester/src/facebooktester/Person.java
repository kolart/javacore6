
package facebooktester;


public class Person extends Man {


    @Override
    public String toString() {
        return "Person{" + "email=" + email + ", phone=" + phone + '}';
    }
    
    protected String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    protected String phone;
    
    Person(String name, int age, String email, String phone)
    {
        super(name, age);
        this.email = email; 
        this.phone = phone;
}
        
    }

    

