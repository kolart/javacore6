
package facebooktester;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;


public class MyFacebook {
   private List<Person>myfriends;
   MyFacebook(){
   this.myfriends = new ArrayList<Person>(); 
   }
   public void addPerson(Person p)
   {
    this.myfriends.add(p);
   }
   public Person getPerson(int number)
   {
       Person p = this.myfriends.get(number);
       return p;
   }
}
