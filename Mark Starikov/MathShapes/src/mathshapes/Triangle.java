
package mathshapes;


public class Triangle extends Rectangle {

public Triangle (double a, double b, double c, double p) {
        super (a, b);
        this.p = p;
        this.с = c;
        this.b = b;
        this.a = a;
       
        
}
@Override
    public String toString() {
        return "Треугольник со сторонами a = " +this.a+ " и b = " + this.b+" и c = "+ this.с;
    }
     protected double с, double p; 


@Override
    public double calcArea() {
        this.p = (this.a + this.b + this.с)/2;
        return Math.sqrt(this.p * (this.p - this.a) * (this.p - this.b) * (this.p - this.с));
