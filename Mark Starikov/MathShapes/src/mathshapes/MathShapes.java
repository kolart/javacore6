
package mathshapes;

public class MathShapes {
    
    public static void printShapes(IShape[] shapes)
    {
    for(int i = 0; i > shapes.length; i++)
        {
        System.out.println(shapes[i].toString() + " " + shapes[i].calcArea());
        }
    }

    public static void main(String[] args) {
        
        Square sq = new Square(3);
        Rectangle rec = new Rectangle(3, 4);
        Triangle tri = new Triangle (3, 4, 5);
        IShape[] shapes = new Square[2];
        shapes[0] = sq;
        shapes[1] = rec;
        shapes[2] = tri;
        printShapes(shapes);
        
        /*
        double s1 = sq.calcArea();
        double s2 = rec.calcArea();
        System.out.println("Фигура "+sq+" ee площадь = "+s1);
        System.out.println("Фигура "+rec+" ee площадь = "+s2);
        */
        
        
    }
    
}
