
package jdbcconnectiontest;

//сначала поля для этого класса - столбцы из таблицы
//потом наследование
// потом конструктор 
//потом служебные методы
public class Employee extends Man {

    public Employee(String tabNumber, double salary, String Fname, String phone) {
        super(Fname, phone);
        this.tabNumber = tabNumber;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return super.toString() + "Employee{" + "tabNumber=" + tabNumber + ", salary=" + salary + '}';
    }

    public String getTabNumber() {
        return tabNumber;
    }

    public void setTabNumber(String tabNumber) {
        this.tabNumber = tabNumber;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
    String tabNumber;
    double salary;
    
    
}
