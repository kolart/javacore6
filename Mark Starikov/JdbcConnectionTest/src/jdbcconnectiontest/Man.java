
package jdbcconnectiontest;

//правило создания - конструктор
//правило отображения не экране -метод ToString()
//-определено правило сравнения элементов
//-правило различения одинаковых элементов
public class Man {

    //обязятельно все поля из таблицы в базе данных
    @Override
    public String toString() {
        return "Man{" + "firstName=" + firstName + ", phone=" + phone + '}';
    }

    public String getFirstName() {
        return firstName;
    }

    private void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    private String firstName;
    private String phone;
    private Man (){}
    public Man(String Fname, String phone)
    {
        this.firstName = Fname;
        this.phone = phone;
    }
}
