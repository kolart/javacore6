
package stringexample1;

public class StringExample1 {

    public static void main(String[] args) {
        //String
        //Stringbuilder
        String str = "Привет, мир!";
        //         012345678901
        //строка это набор букв, есть возможность
        //обратиться к каждой букве по ее номеру
        //нумерация начинается с нуля
        //все функции, которые нужны для работы со
        //строками хранятся внутри строки


        int index = 0;
        System.out.println(str.charAt(index));
        index = 11;
        System.out.println(str.charAt(index));
        int n = str.length();
        System.out.println("Длинна строки \t (колличество букв) \t \""+str+"\" равна"+n);
        for(int i = str.length()- 1; i >=0; i--)
        {
            System.out.print(str.charAt (i)+"\n");
            //System.out.print(str.charAt (i)+'\n');
        }
        char k  = 'a'; //латинская буква
        str = str+" Это я - Вася! ";
        System.out.println(str);
        for(int i = 0; i <=str.length() - 1; i++)
        {
            char ch = str.charAt(i);
            if (ch == k)
            {
                System.out.println("найдено");
            }
        }
            //String char StringBuilder

        String str1 = new String ("hello");
        String str2 = new String ("hello");
        if (str1 == str2)
        {
            System.out.println("строка "+str+" 'равна'"+str2+"'");
        }else
        {
            System.out.println("строка "+str+" 'НЕравна'"+str2+"'");
        }
        // тип для работы с отдельным символом (разных кодировок)
        for(int i = 0; i <=255; i++)
        {
            char CH = (char)i;
            if (i%16 == 0)
            {
                System.out.println(CH);
            }
            else
            System.out.print(CH);  
        }
        //Hello, world! Это Я - Вася!
        //колличество слов = колличество пробелов + 1
        //charAt(i) != ' ' && charAt(i + 1) == ' '
        int counter = 0;
            for (int i = 0; i <= str.length()-1; i++)
        {
            if (str.charAt(i) == ' ')
            {
               counter++;
            }
               
            }
            System.out.println();
            System.out.println("строка = '"+str+"'");
            System.out.println("Колличество слов = "+ (counter + 1) );
            System.out.println();
            String [] words = str.split(" ");
            int N = words.length;
            System.out.print(N);
        }
       
        
    }



