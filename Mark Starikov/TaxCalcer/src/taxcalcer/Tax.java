/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taxcalcer;

/**
 *
 * @author Марк
 */
public class Tax {
    double pn; //18
    double military;//1,5
    double pension1;// 2 
    double pension2;// 22
    double prof;// 2
    double social;// 1
    // правило заполнения начальными значениями 
    // если такое правило есть то оно называется конструктором
    // означает, что у этого понятия есть конструктор = функция которая
    // созлает объект в начальном - безопасном сотстоянии
    // constructor
    // имя конструктора четко определено
    // имя класса () и скобки = потому что функция
    public Tax()
    {
        pn = 0;
        military = 0;
        this.pension1 = 0;
        this.pension2 = 0;
        this.prof = 0;
        social = 0;
                
    }
    public Tax(double salary)
    {
       this.pn = salary * 0.18;
       this.military = salary * 0.015;
       this.pension1 = salary * 0.02;
       this.pension2 = salary * 0.22;
       this.prof = salary * 0.02;
       this.social = salary * 0.01;
    }

    @Override
    public String toString() {
        return "Tax{" + "pn=" + pn + ", military=" + military + ", pension1=" + pension1 + ", pension2=" + pension2 + ", prof=" + prof + ", social=" + social + '}';
    }
    
    public double sum()
    {
        return pn + military +  pension1 + pension2 + prof + social;
    }
    
}
