
package jdbcsqliteconnectiontest1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
public class JDBCSqliteConnectionTest1 {

 
    public static void main(String[] args) {
        
        try {
            Class.forName("org.sqlite.JDBC");
            Connection conn = null;
            conn = DriverManager.getConnection("jdbc:sqlite:C:\\MyData\\test.db");
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery("SELECT ID, B_YEAR, LAST_NAME, FIRST_NAME FROM MAN");
            while (rs.next () )
            {
                int intDigit = rs.getInt("ID");
                int intValue = rs.getInt("B_YEAR");
                String strValue = rs.getString("LAST_NAME");
                String strSign = rs.getString("FIRST_NAME");
                System.out.println("id = " +intDigit+ "Год рождения" +intValue+ "Фамилия" +strValue+ "Имя" +strSign);
                
            }
            int count =0;
            while (rs.next()) {
                count++;
                String strValue = rs.getString("NAME");
                int intValue = rs.getInt("ID");
                int intAge = rs.getInt("AGE");
                int intSalary = rs.getInt("SALARY");
                String strPosit = rs.getString("POSITIONS");
                System.out.println("Position " + count + " |ID = " + intValue+ " |Name  = " + strValue +  "|Age = "+ intAge+ " |Salary "+ intSalary + " |On position " + strPosit);

            }
            System.out.print("Quantity of employers " + count);
        
        } catch (ClassNotFoundException ex) {
           System.out.println("Не подключен файл драйвера sqlite-jdbc.jar ");
        } catch (SQLException ex) {
            System.out.println("Не найден драйв-файл базы данных, проверьте пути");
        }
    }
    
}
