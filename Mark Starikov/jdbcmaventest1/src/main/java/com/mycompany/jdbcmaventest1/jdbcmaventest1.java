
package com.mycompany.jdbcmaventest1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class jdbcmaventest1 {

  public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("Hello from jdbcmaventest1");
        try {
            //регистрация файла драйвера
            Class.forName("org.sqlite.JDBC");
            //создание соединения
            Connection conn = null;
            conn = DriverManager.getConnection("jdbc:sqlite:C:\\MyData\\test_skj.db");
           

            //создание команды
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery("SELECT\n" +
"MEN.FIRST_NAME, SALARY, JOBS.JOB_TITLE\n" +
"FROM\n" +
"EMPLOYEE LEFT JOIN MEN ON EMPLOYEE.ID_MEN = MEN.ID\n" +
"LEFT JOIN JOBS ON EMPLOYEE.ID_JOBS = JOBS.ID");
            
            //выполнение команды
            while( rs.next() )// пока есть следующая
            {
                String fname = rs.getString("FIRST_NAME");
                double salary = rs.getDouble("SALARY");
                
               //
               counter++;
               Employee emp = new Employee(String.valueOf(counter), salary, fname, null);
               
            }
            
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(jdbcmaventest1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(jdbcmaventest1.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
