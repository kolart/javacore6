
package cwtask4;


public class CWTask4 {
    
    private static int digitN(int number, int N)
    {
        int K = (int) ( Math.ceil(Math.log10(Math.abs(number)+ 0.5)) );
        if (K > N )
            return -1;
        else 
            return K;
            
    }

    public static void main(String[] args) {
    
        int number = 3249;
        int N = 4;
        int Count = digitN(number, N);
        if (Count == N)
        System.out.print("Колличесто цифр числа = " +Count);
        else
            System.out.println("Колличесто цифр числа = "+number+"отличается от"+N);
        
      
    }
    
}
