public abstract class Man extends IEmployee{
    public String name;
    public int age;
     Man (String title, String name, int age){
         super(title);
         this.name = name;
         this.age = age;
     }
}
