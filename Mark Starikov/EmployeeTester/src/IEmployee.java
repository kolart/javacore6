public abstract class IEmployee {

    public abstract double calcSalary();

    String positionTitle;

    public IEmployee(String title) {
        this.positionTitle = title;
    }
    @Override
    public String toString (){
        return "positionTitle, salary = (" + positionTitle + " " + calcSalary() +")";
    }
}