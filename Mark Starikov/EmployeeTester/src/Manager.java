public class Manager extends Man{

    double base;
    double bonus;
    Manager(String title, String name, int age, double base, double bonus){
         super(title, name, age);
         this.base = base;
         this.bonus = bonus;
    }

    @Override
    public double calcSalary() {

        double salary  = base+bonus;
        return salary;
    }
}