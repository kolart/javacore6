public class Developer extends Man {
    double hours;
    double costHour;

    Developer(String title, String name, int age, double hours, double costHour) {
        super (title, name, age);
        this.hours = hours;
        this.costHour = costHour;
    }

    @Override
    public double calcSalary() {

        double salary  = hours*costHour;
        return salary;
    }
}
