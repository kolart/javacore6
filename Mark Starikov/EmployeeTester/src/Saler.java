public class Saler extends Man{
    double base;
    double percent;
    double volume;
    Saler(String title, String name, int age,double base, double percent, double volume){
        super(title, name, age);
        this.base = base;
        this.percent = percent;
        this.volume = volume;
    }

    @Override
    public double calcSalary() {

        double salary = base+volume*percent;
        return salary;
    }
}
