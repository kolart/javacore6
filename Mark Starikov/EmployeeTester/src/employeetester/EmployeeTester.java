import java.util.ArrayList;
import java.util.List;

public class EmployeeTester {
    public static void main(String[] args) {
        List<IEmployee> employees = new ArrayList<>();
        employees.add(new Manager("director", "Vasyl", 31, 1000, 1500));
        employees.add(new Accounter("accounter", "Mariya", 29, 1000));
        employees.add(new Saler("saler", "Andrio", 35, 300, 0.03, 100000));
        employees.add(new Developer("senior", " Serhio", 25, 160, 10));
        for (IEmployee e : employees) {
            System.out.println(e);
        }
    }
}
