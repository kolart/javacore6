/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mathshapes1;

/**
 *
 * @author NP
 */
public class Rectangle1 extends Square{
    

    public Rectangle1(double a, double b) {
        super(a);
        this.b = b;
    }
    protected double b;

    @Override
    public String toString() {
        return "Прямоугольник со сторонами а +" + this.a+"и b=" + b;
    }

    @Override
    public double CalcArea() {
        return this.a * this.b;
    }
    
}
