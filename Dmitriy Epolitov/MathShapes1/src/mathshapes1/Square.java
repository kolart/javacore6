
package mathshapes1;


public class Square implements IShape {
   

     Square(double a) {
        this.a = a;
    }
protected double a;

    @Override
    public String toString() {
        return "квадрат со стороной" + "a=" + a;
    }
    @Override
    public double CalcArea() {
        return a * a;
    }
}
