/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication2;

import java.util.Scanner;

/**
 *
 * @author Дмитрий
 */
public class Calcformula2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic her
        System.out.println("Введите площадь пирамиды S");
        System.out.println("Введите высоту пирамиды H");
        Scanner in = new Scanner (System.in);
        double  S = in.nextDouble();
        double  H = in.nextDouble();
        if (S <= 0)
        {
            System.out.println("Введенно неверное значение площади");
            return;
        }
        if (H <= 0)
        {
            System.out.println("Введенно неверное значение высоты");
            return;
        }
        double volume;
        volume = S * H / 3;
        System.out.println("Обьем пирамиды равен="+volume);

    }
    
}
