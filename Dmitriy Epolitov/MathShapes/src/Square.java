/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author NP
 */
public class Square implements Shape{
    private double a;

    @Override
    public String toString() {
        return "квадрат со стороной" + "a=" + a + '}';
    }

    public Square(double a) {
        this.a = a;
    }
    protected double a;
    @Override
    public double calcAera(){
        return a * a;
    }
    
}
