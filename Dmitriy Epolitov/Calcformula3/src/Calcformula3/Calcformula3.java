/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calcformula3;

import java.util.Scanner;

/**
 *
 * @author Дмитрий
 */
public class Calcformula3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("Введите радиус круга чтобы получить его площадь");
        Scanner in = new Scanner (System.in);
        double r = in.nextDouble();
        if ( r <= 0 )
        {
            System.out.println("Введенно неверное значение радиуса r не может "
                    + "быть отрицательным или равен нулю");
        return;
        }
        double circle = Math.PI * r * r;
        System.out.println("Площадь круга равна"+circle);
        
                
    }
    
}
