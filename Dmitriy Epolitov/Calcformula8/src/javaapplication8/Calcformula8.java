/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication8;

import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Дмитрий
 */
public class Calcformula8 {
    public static void main(String[] args) { 
            System.out.println("Введите значение радиуса треугольника");
            Scanner in = new Scanner (System.in);
            double r = in.nextDouble();
            if (r <= 0){
                System.out.println("Введено неверный радиус");
                return;
            }
            double radius = Math.PI * r;
            System.out.println("Площадь треугольника равна"+radius);
            
        }
        
    }
