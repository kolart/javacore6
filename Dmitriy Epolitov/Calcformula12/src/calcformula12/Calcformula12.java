/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calcformula12;

import java.util.Scanner;

/**
 *
 * @author Дмитрий
 */
public class Calcformula12 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("Введите сторону а");
        System.out.println("Введите высоту h");
        Scanner in = new Scanner(System.in);
        double a = in.nextDouble();
        double h = in.nextDouble();
        if ( a <= 0 ){
            System.out.println("Введено неверное значение стороны а");
            return;
        }
        if ( h <= 0 ){
            System.out.println("Введено неверное значение высоты");
            return;
            
        }
        double result = a * h / 2;
        System.out.println("Площадь треугольника по стороне и опущеной на нее высоте равняется"+result);
        
        
    }
    
}
