/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author NP
 */
public class ABITURIENTS extends MAN{
    private String ID_MAN;

    public String getID_MAN() {
        return ID_MAN;
    }

    public void setID_MAN(String ID_MAN) {
        this.ID_MAN = ID_MAN;
    }

    @Override
    public String toString() {
        return "ABITURIENTS{" + "ID_MAN=" + ID_MAN + '}';
    }

    public ABITURIENTS(String ID_MAN, String FIRST_NAME, String LAST_NAME, String SEKOND_NAME, String B_YEAR) {
        super(FIRST_NAME, LAST_NAME, SEKOND_NAME, B_YEAR);
        this.ID_MAN = ID_MAN;
    }
}
