


public class MAN {
    private String FIRST_NAME;
    private String LAST_NAME;
    private String SEKOND_NAME;
    private String B_YEAR;

    @Override
    public String toString() {
        return "MAN{" + "FIRST_NAME=" + FIRST_NAME + ", LAST_NAME=" + LAST_NAME + ", SEKOND_NAME=" + SEKOND_NAME + ", B_YEAR=" + B_YEAR + '}';
    }
    private MAN(){}
    
    
    public MAN(String FIRST_NAME, String LAST_NAME, String SEKOND_NAME, String B_YEAR)
    {
        this.FIRST_NAME = FIRST_NAME;
        this.LAST_NAME = LAST_NAME;
        this.SEKOND_NAME = SEKOND_NAME;
        this.B_YEAR = B_YEAR;
    }

    public String getFIRST_NAME() {
        return FIRST_NAME;
    }

    public void setFIRST_NAME(String FIRST_NAME) {
        this.FIRST_NAME = FIRST_NAME;
    }

    public String getLAST_NAME() {
        return LAST_NAME;
    }

    public void setLAST_NAME(String LAST_NAME) {
        this.LAST_NAME = LAST_NAME;
    }

    public String getSEKOND_NAME() {
        return SEKOND_NAME;
    }

    public void setSEKOND_NAME(String SEKOND_NAME) {
        this.SEKOND_NAME = SEKOND_NAME;
    }

    public String getB_YEAR() {
        return B_YEAR;
    }

    public void setB_YEAR(String B_YEAR) {
        this.B_YEAR = B_YEAR;
    }
    
}
