/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author NP
 */
public class TEACHERS extends MAN{
   private String ID_MAN;
   private String ITEM;

    public String getID_MAN() {
        return ID_MAN;
    }

    public void setID_MAN(String ID_MAN) {
        this.ID_MAN = ID_MAN;
    }

    public String getITEM() {
        return ITEM;
    }

    public void setITEM(String ITEM) {
        this.ITEM = ITEM;
    }

    @Override
    public String toString() {
        return "TEACHERS{" + "ID_MAN=" + ID_MAN + ", ITEM=" + ITEM + '}';
    }

    public TEACHERS(String ID_MAN, String ITEM, String FIRST_NAME, String LAST_NAME, String SEKOND_NAME, String B_YEAR) {
        super(FIRST_NAME, LAST_NAME, SEKOND_NAME, B_YEAR);
        this.ID_MAN = ID_MAN;
        this.ITEM = ITEM;
    }
    
}
