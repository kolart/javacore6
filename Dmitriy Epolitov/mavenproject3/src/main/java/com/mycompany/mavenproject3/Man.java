
package com.mycompany.mavenproject3;


public class Man {
  private String firstName;
   private String phone;

    public String getFirstName() {
        return firstName;
    }

    private void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Man{" + "firstName=" + firstName + ", phone=" + phone + '}';
    }
   private Man (){}
   public Man (String Fname, String phone)
   {
       this.firstName = Fname;
       this.phone = phone;
   }
}
