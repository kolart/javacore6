
package com.mycompany.mavenproject3;


public class Employee extends Man{
    String tabNumber;
    double salary; 

    public Employee(String tabNumber, double salary, String Fname, String phone) {
        super(Fname, phone);
        this.tabNumber = tabNumber;
        this.salary = salary;
    }
       

    @Override
    public String toString() {
        return super.toString()+"Employee{" + "tabNumber=" + tabNumber + ", salary=" + salary + '}';
    }
        

    public String getTabNumber() {
        return tabNumber;
    }

    public void setTabNumber(String tabNumber) {
        this.tabNumber = tabNumber;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
   String tabNumber;
       double salary; 
}

