
package jdbcsqlliteconectiontest1;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JDBCSqlliteconectiontest1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
         try {
            Class.forName("org.sqlite.JDBC");
            Connection conn = null;
             conn = DriverManager.getConnection("jdbc:sqlite:C:\\MyData\\test.db");
             Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery("SELECT FIRST_NAME, ID FROM MAN");
         
            while(rs.next())
            { 
                String strVAlue = rs.getString("FIRST_NAME");
            
                    System.out.println(strVAlue);
                    }} 
            catch (ClassNotFoundException ex) 
        {
            System.out.println("Не подключен файл драйвера ");
        } 
        catch (SQLException ex) 
        {
           System.out.println("Не найден файл базы данных.Проверьте пути");
        }
       }
    }
    

