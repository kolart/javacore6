package Homie.box.sample.class1707;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyWindowCalc extends JFrame implements ActionListener
{
    private JLabel lblMessage;
    private JTextField txtInput;
    private JButton btnCalc;
    private JLabel lblResult;

    public MyWindowCalc()
    {
        this.setSize(400, 100);
        this.setTitle("Square of ");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new FlowLayout());

        lblMessage= new JLabel("input your money");
        txtInput= new JTextField("        ");
        btnCalc = new JButton("Calculate");
        lblResult = new JLabel("Result = ");


        this.add(lblMessage);
        this.add(txtInput);
        this.add(btnCalc);
        this.add(lblResult);
        btnCalc.addActionListener(this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        String tetV = txtInput.getText();
        this.setTitle("Inputed "+ tetV);
    }
}
