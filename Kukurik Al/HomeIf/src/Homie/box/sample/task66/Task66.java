package Homie.box.sample.task66;

public class Task66 {
    public int funk(int n){
        int res = n;
        boolean flag = false;
        while (n > 0) {
            res = n % 10;
            if (res == 2){
                flag = true;
            }
            n = n / 10;
        }System.out.println(flag);
        return n;
    }
}
