package Homie.box.sample.taskStr9;

public class TaskStr9 {
    public void funk(String str){
        char symb;
        System.out.println(str);
        for (int i = 0; i < str.length(); i++) {
            symb = (char)str.charAt(i);
            if (symb>=65 && symb<=90){
                symb=Character.toLowerCase(symb);
            }else if (symb>=97 && symb<=122){
                symb=Character.toUpperCase(symb);
            }
            System.out.print(symb);
        }
    }
}
