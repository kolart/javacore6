package Homie.box.sample.taskStr12;

public class TskStr12 {
    public void fink(String str) {
        char ch;
        for (int i = 0; i < str.length() ; i++) {
            ch = (char) str.codePointAt(i);
            System.out.print(ch+" ");
            System.out.print(str.charAt(i) + '\n' + " ");
            System.out.print((str.codePointAt(i)) + " ");
            System.out.println(String.format("\\u%04x", (int) ch));
        }
    }
}
