package Homie.box.sample.task43;

public class Task43 {
    public void funk() {

        for (int i = 1; i <= 10; i++) {
            System.out.print("Table of multiplication of " + i);
            System.out.println(" " );
            for (int j = 1; j <= 10; j++) {
                System.out.println(i + " * " + j + " = " +i*j+";");
            }
        }
    }
}
