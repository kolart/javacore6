package Homie.box.sample.task70;

public class Task70 {
    public double funk(double dist, double pers) {
        int day = 0;
        while (dist <= 200) {
            dist = dist + dist * pers;
            day++;
        }
        return day;
    }
}
