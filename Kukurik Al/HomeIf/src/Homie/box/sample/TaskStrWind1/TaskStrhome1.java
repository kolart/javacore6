package Homie.box.sample.TaskStrWind1;

import Homie.box.sample.taskStr26.TaskStr26;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TaskStrhome1 extends JFrame implements ActionListener {
    private JLabel lblCondition;
    private JLabel lblMessage;
    private JTextField txtInput;
    private JButton btnCalc;
    private JLabel lblResult;

    public TaskStrhome1() {
        this.setSize(1000, 300);
        this.setTitle(" Work with string ");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new FlowLayout());

        lblCondition = new JLabel(
                "Введите строку, содержащую по крайней мере один символ пробела. Выведем подстроку, \n" +
                "расположенную между первым и вторым пробелом исходной строки. Если строка \n" +
                "содержит только один пробел, то выведем пустую строку");
        lblMessage = new JLabel("input your string");
        txtInput = new JTextField("                              ");
        btnCalc = new JButton("Process ");
        lblResult = new JLabel("Result = ");

        this.add(lblCondition);
        this.add(lblMessage);
        this.add(txtInput);
        this.add(btnCalc);
        this.add(lblResult);
        btnCalc.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String tetV = txtInput.getText();
        TaskStr26 task = new TaskStr26();
        String rez = task.funk(tetV);
        lblResult.setText(rez);


    }
}
