package Homie.box.sample.task47;

import Homie.box.sample.Checkin;

public class R47 {
    public static void main(String[] args) {
        int a = 0;
        int b = 0;
        boolean ready = false;
        while (!ready) {
            a = (int) Checkin.reaD("a");
            b = (int) Checkin.reaD("b");
            ready = a > 0 && b > 0;
        }
        Tasr47 test = new Tasr47();
        int res = test.func(a, b);
        System.out.println("Result  = " + res);
    }

}
