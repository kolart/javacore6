package Homie.box.sample;

public class Task7 {
    public boolean funk(int n) {
        int fib1 = 1;
        int fib2 = 1;
        int sumF = 0;
        boolean result;
        while (sumF < n) {
            sumF = fib1 + fib2;
            fib1 = fib2;
            fib2 = sumF;
        }
        if (sumF == n) {
            result = true;
        } else {
            result = false;
        }
        System.out.println(result);
        return result;
    }

}

