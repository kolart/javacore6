package Homie.box.sample;

import sample.box.Check;

public class CycleSumTest {
    public static void main(String[] args) {
        for (int i = 10; i >= 0; i--) {
            System.out.print(" " + i);
        }
        System.out.println();
        double a = Check.readD("a");
        double b = Check.readD("b");

        if (b < a) {
            double t = a;
            a = b;
            b = t;
        }
        int sum = 1;
        System.out.println();
        for (double i = a; i <= b; i++) {
            sum = (int) (sum * i);
        }
        System.out.println(sum);
    }


}
