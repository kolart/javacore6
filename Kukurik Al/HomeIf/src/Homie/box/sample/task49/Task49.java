package Homie.box.sample.task49;

public class Task49 {
    public void func(int a, int b) {
        int count = 0;
        for (int i = a; i >= b; i--) {
            System.out.println(i);
            count++;
        }
        System.out.println("Quantity of numbers "+ count);
    }
}
