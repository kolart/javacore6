package Homie.box.sample.task59;

public class Task59 {
    public int func(int n) {
        int res = 1;
        int cont = countDigit(n);
        for (int i = 1; i <= cont; i++) {
            res = 10 * res;
        }
        res = n * (res + 1);
        return res;
    }

    private int countDigit(int z) {
        int cont = 0;
        for (int i = 1; z > 0; i++) {
            z = z / 10;
            cont++;
        }
        return cont;
    }
}
