package Homie.box.sample;

public class ForInClass {
    public static void main(String[] args) {
        int a = 1;
        int b = 10;
        int step = 0;
        int i = a;
        while (a > i) {
            System.out.println("Step of cycle " + step);
            System.out.println(i);
            step++;
        }
        for (int j = 11; j < 20; j = j + 2) {
            System.out.println(j);
        }
        i = 11;
        while (i <= 19) {
            System.out.println(i);
            i+=2;
        }
    }
}
