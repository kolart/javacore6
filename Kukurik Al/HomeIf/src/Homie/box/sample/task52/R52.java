package Homie.box.sample.task52;

import sample.box.Check;

public class R52 {
    public static void main(String[] args) {
        int a=0;
        boolean ready = false;
        while (!ready){
            a = (int) Check.readD("a");
            ready = a>0;
        }
        Task52 test = new Task52();
        int res = test.funk(a);
        System.out.println( res );
    }
}
