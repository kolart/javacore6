package Homie.box.sample.task48;

public class Task48 {
    public void func(int a, int b) {
        int count = 0;
        for (int i = a; i <= b; i++) {
            System.out.println(i);
            count++;
        }
        System.out.println("Quantity of numbers "+ count);
    }
}
