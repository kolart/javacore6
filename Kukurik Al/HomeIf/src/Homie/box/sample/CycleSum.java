package Homie.box.sample;

public class CycleSum {
    public static void main(String[] args) {
        int s = 0;
        for (int i = 0; i <= 10; i++) {
            s = s + i;
        }
        System.out.println("sum of " + s);
        int i = 0;
        s=0;
        while ( i<=10){
            i++;
            if (i%2==0){
                s=s+i;
            }
        }
        System.out.println("sum of " + s);
    }
}
