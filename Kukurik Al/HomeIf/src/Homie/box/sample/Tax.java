package Homie.box.sample;

public class Tax {
    double pn;//18
    double pension1;//2
    double pension2;//22
    double prof;//2
    double social;//1
    double military;//1,5

    public Tax(){
        pn= 0;
        military =0;
        this.pension1 =0;
        this.pension2= 0;
        this.prof= 0;
        this.social= 0;
    }
    public Tax(double salary)
    {
        this.pn = salary*0.18;
        this.military =salary*0.015;
        this.pension1 = salary*0.02;
        this.pension2 = salary*0.22;
        this.prof = salary*0.02;
        this.social= salary*0.01;
    }

    @Override
    public String toString() {
        return "Tax{" +
                "pn=" + pn +
                ", pension1=" + pension1 +
                ", pension2=" + pension2 +
                ", prof=" + prof +
                ", social=" + social +
                ", military=" + military +
                '}';
    }
}
