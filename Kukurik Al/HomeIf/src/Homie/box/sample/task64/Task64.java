package Homie.box.sample.task64;

public class Task64 {
    public int funk(int n) {
        int res = n;
        int count = 0;
        int sum = 0;
        while (n > 0) {
            System.out.println(n);
            count++;
            res = n % 10;
            n = n / 10;
            sum = sum + res;
            System.out.println("Quantity " + count + "  sum of " + sum);
        }
        return n;
    }
}
