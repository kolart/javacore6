package Homie.box.sample.task57;

import Homie.box.sample.task56.Task56;

public class Task57 {
    public int funk (int n){
        Task56 test = new Task56();
        int res =0;
        for (int i = 1; i <= n; i++) {
            int a= test.funk(i);
            res = res+a;
        }
        return res;
    }
}
