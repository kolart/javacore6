package Homie.box.sample;

public class TaxCalc {
    public static Tax calc (double salary){
        Tax  taxes = new Tax(salary);
        return taxes;
    }
}

class Relize{
    public static void main(String[] args) {
        double salary = 6000;
        Tax myTax = TaxCalc.calc(salary);
        System.out.println("My taxes "+myTax);
    }
}