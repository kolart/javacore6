package Homie.box.sample.taskStrWind2;


import Homie.box.sample.taskStr29.TaskStr29;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

    public class TaskStrWind2 extends JFrame implements ActionListener {
        private JLabel lblCondition;
        private JLabel lblMessage;
        private JTextField txtInput;
        private JButton btnCalc;
        private JLabel lblResult;

        public TaskStrWind2() {
            this.setSize(1000, 300);
            this.setTitle(" Work with string ");
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            this.setLayout(new FlowLayout());

            lblCondition = new JLabel(
                    "Дана строка, состоящая из русских слов, набранных заглавными буквами и разделенных \n" +
                            "пробелами (одним или несколькими). Найти количество слов, которые начинаются и \n" +
                            "заканчиваются одной и той же буквой.");
            lblMessage = new JLabel("input your string");
            txtInput = new JTextField("");
            btnCalc = new JButton("Process ");
            lblResult = new JLabel("Result = ");

            this.add(lblCondition);
            this.add(lblMessage);
            this.add(txtInput);
            this.add(btnCalc);
            this.add(lblResult);
            btnCalc.addActionListener(this);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            String tetV = txtInput.getText();
            TaskStr29 task = new TaskStr29();
            String rez = task.funk(tetV);
            lblResult.setText(rez);


        }
    }
