package Homie.box.sample.task58;

import Homie.box.sample.task56.Task56;

public class Task58 {
    public double funk (int n){
        Task56 test = new Task56();
        double res =0;

        for (double i = 1; i <= n; i++) {
            double a= (double) test.funk((int) i);
            res = res+(1/a);

        }
        return res;
    }
}