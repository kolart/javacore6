package Homie.box.sample.taskStr28;

public class TaskStr28 {
    public void funk(String base) {
        int count = 0;
        int word = 0;
        for (int i = 0; i < base.length(); i++) {
            if ((base.charAt(i) >= 'А' && base.charAt(i) <= 'Я') || (base.charAt(i) >= 'а' && base.charAt(i) <= 'я')) {
               count++;
            }
            if (base.charAt(i) == ' ') {
                count =0;
            }
            if (count ==1){
                word++;
            }
        }
        System.out.println("Quantity of words " + word);
    }
}
