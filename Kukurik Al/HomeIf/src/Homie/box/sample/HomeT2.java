package Homie.box.sample;

import sample.box.Check;

public class HomeT2 {
    public static void main(String[] args) {
        double a = Check.readD("a");
        double b = Check.readD("b");
        double c = Check.readD("c");
        double d = Check.readD("d");
        double f = Math.sqrt(Math.pow(a - b, 2) + Math.pow(c - d, 2));
        System.out.println(" Distance = " + f);
    }
}
