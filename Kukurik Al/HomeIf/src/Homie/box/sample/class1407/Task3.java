package Homie.box.sample.class1407;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        System.out.println("input number ");
        Scanner input = new Scanner(System.in);
        String integer = input.nextLine();
        int count = 0;
        String[] parts = integer.split(".");

        for (int i = 0; i < integer.length(); i++) {
            char ch = integer.charAt(i);
            if (ch == '0') {
                count++;
            }
        }
        System.out.println("Quantity of zero " + count);
    }
}
