package Homie.box.sample;

import sample.box.Check;

public class LastMiss {
    void func(){
        System.out.println("input coordinates of point");
        double x = Check.readD("x");
        double y = Check.readD("y");
        System.out.println("input coordinates of top left rectangle");
        double x1 = Check.readD("x1");
        double y1 = Check.readD("y1");
        System.out.println("input coordinates of low right rectangle");
        double x2 = Check.readD("x2");
        double y2 = Check.readD("y2");
        if (x1<x && x<x2 && y2<y && y<y1){
            System.out.println("point in rectangle");
        }
        else {
            System.out.println("point not in rectangle");
        }
    }
}
class R17{
    public static void main(String[] args) {
        LastMiss test = new LastMiss();
        test.func();
    }
}