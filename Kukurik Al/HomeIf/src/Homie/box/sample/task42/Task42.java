package Homie.box.sample.task42;

public class Task42 {
    public void func( double i, double n) {

        double f = 0;
        for (; i <= n; i++) {
            f = (i * 1.8) + 32;
            System.out.println(i + " in C  equal " + f + " F ");
        }
    }
}
