package Homie.box.sample.task46;

import sample.box.Check;

public class R46 {
    public static void main(String[] args) {

        int a = 0;
        int b = 0;
        boolean ready = false;

        while (!ready) {
             a = (int) Check.readD("a");
             b = (int) Check.readD("b");
             ready = a < b;
        }

        Task46 test = new Task46();
        int res = test.func(a, b);
        System.out.println("Sum of elements from  a  to  b  = " + res);
    }
}
