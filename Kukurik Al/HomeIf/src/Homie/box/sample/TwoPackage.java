package Homie.box.sample;

public class TwoPackage {
    public double pack2() {
        double a = Checkin.reaD("a");
        double b = Checkin.reaD("b");
        if (a <= 0 || b <= 0) {
            System.out.println("Packages cant be with negative or zero value");
            return pack2();
        }
        if (a < b) {
            System.out.println("Package " + b + " heavier than " + a);
            return b;
        } else if (a > b) {
            System.out.println("Package " + a + " heavier than " + b);
            return a;
        } else {
            System.out.println("Packages are equal");
            return a;
        }
    }
}

class R3 {
    public static void main(String[] args) {
        TwoPackage eq = new TwoPackage();
        eq.pack2();
    }
}
