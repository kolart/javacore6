package Homie.box.sample;

import sample.box.Check;

public class TriCircle {
    public void funct() {
        System.out.println("Radius of circle");
        double a = Check.readDPositive("a");
        System.out.println("side of triangle");
        double b = Check.readDPositive("b");
        double d = (a * Math.sqrt(3)) / 3;
        if (a >= d) {
            System.out.println("Triangle in circle");
        } else {
            System.out.println("Triangle not in circle");
        }
    }
}

class R8 {
    public static void main(String[] args) {
        TriCircle test = new TriCircle();
        test.funct();
    }
}