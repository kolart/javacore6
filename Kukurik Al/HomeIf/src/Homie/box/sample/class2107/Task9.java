package Homie.box.sample.class2107;

public class Task9 {
    public void funk(String str){
        String reverse = "";

        for (int i = str.length()-1; i >=0 ; i--) {
            reverse =reverse+str.charAt(i);
        }
        System.out.println("Reverse of "+ str + " is "+ reverse);
    }
}
