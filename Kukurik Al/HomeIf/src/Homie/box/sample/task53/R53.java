package Homie.box.sample.task53;

import Homie.box.sample.Checkin;

public class R53 {
    public static void main(String[] args) {
        int a = 0;
        int n = 0;
        boolean read = false;
        while (!read) {
            a = (int) Checkin.reaD("a");
            n = (int) Checkin.reaD("n");
            read = a > 0 && n > 0;
        }
        Task53 test = new Task53();
       int res= test.func(a,n);
        System.out.println(res);
    }
}
