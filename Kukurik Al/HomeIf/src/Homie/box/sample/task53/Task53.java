package Homie.box.sample.task53;

public class Task53 {
    public int func (int a, int n){
        int sqr = 1;
        for (int i = 1; i <= n; i++) {
            sqr = sqr*a;
        }
        return sqr;
    }
}
