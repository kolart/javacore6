
package cwtask04;


public class CWTask04 {

    
    private static int digitN(int number, int N)
    {
        int K = (int)(  Math.ceil(Math.log10(Math.abs(number) + 0.5)) ) ;
        if ( K > N )
            return -1;
        else
            return K;
    }
    

    public static void main(String[] args) {
        // три варианта решения. В зависимости от того, на чем делаем акцент или фокус
        //условие: считаем количество цифр в том числе, которое ввел пользователь
        //вариант 1 = главное синтаксис функции
        //решение 
        int number = 324;
        int N = 4;
        int Count = digitN(number,N);//number мое число, 4 = мои ожидания (проверяю на четырехзначность
        if (Count == N)
        System.out.println("Количество цифр числа = "+Count);
        else
             System.out.println("Количество цифр в числе "+number+" отличается от "+N);
        
        //вариант 2 = самый известный = это через циклы и операцию деления = повторяем деление на 10 пока есть что делить
        
        //Вариант 3 = чисто программисткий = через строки
        
    }
    
}
