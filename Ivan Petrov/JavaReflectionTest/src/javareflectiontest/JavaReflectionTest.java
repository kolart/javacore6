
package javareflectiontest;

//import java.lang.reflect.*//механимзы программного
//исследования кода
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;


public class JavaReflectionTest {


    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException {
        //список методов класса
        //Список методов класса
        String className = "javareflectiontest.MyUser";
        
        //Class clazz = MyUser.class;
        Class clazz;
        try {
            //аналог new с конструктором по умолчанию
            clazz = Class.forName(className);
            Class superClasses =  clazz.getSuperclass();
            System.out.println("у класса есть родитель "+superClasses); 
            className = clazz.getClass().toString();
            System.out.println("Список методов класса:" + className);
            Method[] methods = clazz.getMethods();
            for(Method method : methods)
            {
                System.out.println(method);
            }
            //
            
             //import java.lang.reflect.Constructor;
            Constructor[] contrs = clazz.getConstructors();
            System.out.println("конструкторы: ");
            for (Constructor function : contrs) {
                System.out.println(function.getName()+" параметры "+function.getParameterCount() );
                
            }
            //
            Field[]  fields =  clazz.getFields();
            Class[] interfaces = clazz.getInterfaces();
            System.out.println("intefaces : ");
            for(Class c : interfaces)
            {
                System.out.println(c);
            }
            //
            System.out.println("вызов метода при помощи рефлексии");
            Method method;
            method = clazz.getClass().getMethod("getEmail");
            String userEmail = (String) method.invoke(clazz);
//
            
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(JavaReflectionTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(JavaReflectionTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
