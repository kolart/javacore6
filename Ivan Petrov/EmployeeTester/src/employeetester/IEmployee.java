package employeetester;
//Man = firstname lastname sex
//Person - inn, phone, e-mail
public abstract class IEmployee implements Comparable<IEmployee> // расширять Person
{
    
     @Override
    public int compareTo(IEmployee o) 
    {
       // return (int)(o.calcSalary() - this.calcSalary());
       
       if (o.calcSalary() > this.calcSalary())
           return 1;
       else        if (o.calcSalary() < this.calcSalary())
           return -1;
       return 0;

         
       
    }
   public abstract double calcSalary();
   String positionTitle;
   //валюты
   public IEmployee(String title)
   {
       this.positionTitle = title;
   }

    @Override
    public String toString() {
        return "positionTitle = "+ positionTitle +", salary = " +  calcSalary();
    }
   
   
}
