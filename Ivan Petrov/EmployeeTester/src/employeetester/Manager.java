
package employeetester;


public class Manager extends IEmployee 
{ 
   
    double base;
    double bonus;

    public Manager(String title, double base, double bonus) {
        super(title);
        this.base = base;
        this.bonus = bonus;
    }

    @Override
    public double calcSalary()
    {
        double salary = base + bonus;
        return salary;
    }

   
    
}
