package employeetester;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class EmployeeTester {


    public static void main(String[] args)
    {
       
        List<IEmployee> employees = new ArrayList<IEmployee>();
        
        employees.add(new Manager("директор", 1000,1500) );
        employees.add(new Acounter("бухгалтер", 500));
        employees.add(new Saler("продажник",300,0.03,100000));
        // employees.add(new Developer("Jun Developer",160,6.25)  );
        // employees.add(new Developer("Developer",160,10)  );
        // employees.add(new Developer("Middle Developer",160,15)  );
        // employees.add(new Developer("Senior Developer",160,25)  );
        
//        for(IEmployee e: employees)
//        {
//            System.out.println(e);
//        }
//        Collections.sort(employees);
//        for(IEmployee e: employees)
//        {
//            System.out.println(e);
//        }
        employees.forEach( e -> System.out.println(e));
        Collections.sort(employees, (o1, o2) -> o2.compareTo(o1));
        employees.forEach( e -> System.out.println(e));


    }
    
}
