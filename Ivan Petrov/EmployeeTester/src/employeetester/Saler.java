
package employeetester;


public class Saler extends IEmployee
{
    double base;
    double percent;
    double volume;
    public Saler(String title, double base, double percent, double volume) {
        super(title);
        this.base = base;
        this.percent = percent;
        this.volume = volume;
    }

    
    @Override
    public double calcSalary()
    {
        double salary = base + percent * volume;
        return salary;
    }
    
}
