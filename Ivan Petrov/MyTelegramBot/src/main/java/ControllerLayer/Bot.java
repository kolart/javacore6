
package ControllerLayer;

//
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.Date;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.generics.LongPollingBot;
import static org.telegram.telegrambots.logging.BotLogger.log;

import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;

//

public class Bot extends TelegramLongPollingBot

{

    
     public static LongPollingBot getBot() {
        return new Bot();
    }
    public Bot()
    {
        super();
    }
    
        //
/**
     * Метод для настройки сообщения и его отправки.
     * @param chatId id чата
     * @param s Строка, которую необходимот отправить в качестве сообщения.
     */
    public synchronized void sendMsg(String chatId, String s) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(chatId);
        sendMessage.setText(s);
        try {
            sendMessage(sendMessage);
        } catch (TelegramApiException e) {
            //log.log(Level.SEVERE, "Exception: ", e.toString());
        }
    }
//
    
    
    @Override
    public String getBotToken() {
      String botToken = "1976862812:AAFye1bnBWDZi3xJHHrfNj81rWbWT4QWLXE";
      return botToken;
    }

    @Override
    public void onUpdateReceived(Update update) {
        
        if (   update != null //пустота от другого - бота - программы
            && update.hasMessage() 
            && update.getMessage().hasText())
        {
            String message_text = update.getMessage().getText();
            if (message_text.equals("/time")) 
            {
             
              Date currentDate = new Date();
              String messageSend = currentDate.toString();
                 
              long chat_id = update.getMessage().getChatId();
              sendMsg(update.getMessage().
                         getChatId().toString(), messageSend);
                
                
            }
            else
                if (message_text.contains("/id"))
                {
                    // /id   345
                  String[] words = message_text.split("/id");
                    
                  String message = "";
                  for(String word : words)
                  {
                      message = message + word.trim();
                  }
	          sendMsg(update.getMessage().getChatId().toString(), message);
                
                    
                    
                }
            else
            if (message_text.equals("/help")) 
            {
                String userID = " user id ";//update.getMessage().getContact().getUserID().toString();
                String username = update.getMessage().getFrom().getUserName();
                 long chat_id = update.getMessage().getChatId();
                 sendMsg(update.getMessage().
                         getChatId().toString(), username+" "+userID+", две команды: /time и /hello");
            }
            else//в ответ отправл¤ю эхом то, что пришло
            {
                
                String message = update.getMessage().getText();
	        sendMsg(update.getMessage().getChatId().toString(), message + "?");
                
            }
        }
    }

    @Override
    public String getBotUsername() {
        String botName = "mybot2021090400_bot";
        return botName;
    }
    
}
