
package stringreplace1;


public class StringReplace1 {


    public static void main(String[] args) {
       
        String input = "road to hell";
        
        //String im-mut-able = неизменяемая в памяти строка
        //сорока
        //заменить все буквы о на ноль
        //версия 1 цикл
        //определить номера буквы o
        int n = input.length();
        char key = 'o';//английская кодировка
        for(int i = 0; i <= n - 1; i++)
        {
            char ch = input.charAt(i);
            if (key == ch)
            {
                System.out.println(i);
            }
        }
        String newStr = "";
        for(int i = 0; i <= n - 1; i++)
        {
            char ch = input.charAt(i);
            
            if (key == ch)
            {
                newStr = newStr + '0';
            }
            else
            {
                newStr = newStr + ch;
            }
        }
        //версия 2 replace
        String newStr2 = newStr.replace('0', 'o');
        System.out.printf("исходная строка \'%s\', результат 1 = \'%s \', результат 2 \'%s\' ",
                input, newStr, newStr2);
        
        //ДЗ1 реализовать тест с кириллицей
        //ДЗ2 оформить решения в виде функций
        
    }
    
}
