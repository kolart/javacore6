
package jdbcconnectiontest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;


public class JdbcConnectionTest {


    public static void main(String[] args)
    {
        
        try {
            // JDBC
            //1. + Подключаем файл драйвера (источник
            // сайт производителя базы данных или maven)
            //2. Регистриуем конкретный класс для работы с базой данных в проекте
            Class.forName("org.sqlite.JDBC");
            //3. Создаем соединение с конретной базой данных
            Connection conn = null;
            conn = DriverManager.getConnection("jdbc:sqlite:C:\\MyData\\test.db");
            //4. выполняем конкретную команду sql
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery("SELECT FIRST_NAME, ID FROM MAN");
            
            while( rs.next() )// пока есть следующая
            {
                String strValue = rs.getString("FIRST_NAME");
                int intValue = rs.getInt("ID");
                System.out.println("имя = "+strValue+", id = "+intValue);
            }
            
        } catch (ClassNotFoundException ex) {
        
            System.out.println("Не подключен файл драйвера sqlite-jdbc.jar");
            
        } catch (SQLException ex) {
            System.out.println("Не найден файл базы данных. Проверьте пути.");
        }
        
    }
    
}
