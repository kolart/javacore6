package mathshapes;


public interface IShape {
    double calcArea();
}
