
package mathshapes;


public class Rectangle extends Square {

    public Rectangle(double a, double b) {
        super(a);
        this.b = b;
        this.a = a;
    }

    @Override
    public String toString() {
        return "Прямоугольник со сторонами a = "+this.a 
                + " и b=" + b;
    }
    protected double b;

    @Override
    public double calcArea() {
        return this.a * this.b; //To change body of generated methods, choose Tools | Templates.
    }
    
}
