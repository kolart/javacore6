
package mathshapes;


public class MathShapes {

    public static void printShapes(IShape[] shapes)
    {
        for(int i = 0;i < shapes.length; i++)
        {
        System.out.println(shapes[i].toString() + " " + shapes[i].calcArea());
        }
    }
    public static void main(String[] args) {
        
        Square sq = new Square(3);
        Rectangle rec = new Rectangle(3, 4);
        IShape[] shapes = new Square[2];
        shapes[0] = sq;
        shapes[1] = sq;
        printShapes(shapes);
        /*
        double s1 = sq.calcArea();
        double s2 = rec.calcArea();
        System.out.println("Фигура "+sq.toString()+" ее площадь = "+s1);
        //Фигура квадрат со стороной 3 ее площадь = 9
        System.out.println("Фигура "+rec+" ее площадь = "+s2);
        //Фигура прямоугольник со стороной 3 и 4 ее площадь = 12
        */
        
    }
    
}
