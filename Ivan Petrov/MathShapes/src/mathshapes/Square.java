
package mathshapes;


public class Square implements IShape {

    public Square(double a) {
        this.a = a;
    }

    @Override // для Object
    public String toString() {
        return "квадрат со стороной " + "a=" + a;
    }
    protected double a;

    @Override // для интерфейса
    public double calcArea() {
        return a * a;
    }
    
}
