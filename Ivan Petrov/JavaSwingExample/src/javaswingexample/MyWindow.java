
package javaswingexample;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

// import javax.swing.*;

public class MyWindow extends JFrame implements ActionListener
{
    private JLabel lblMessage;
    private JTextField txtInput;
    private JButton btnCalc;
    private JLabel lblResult;
    
    public MyWindow()
    {
        this.setSize(400, 100);
        this.setTitle("Площадь квадрата");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        lblMessage = new JLabel("Введите х");
        txtInput = new JTextField("Привет, мир");
        btnCalc = new JButton("Вычислить");
        lblResult = new JLabel("результат = ");
        
  
        this.setLayout( new FlowLayout() );
        this.add(lblMessage);
        this.add(txtInput);
        this.add(btnCalc);
        this.add(lblResult);

        btnCalc.addActionListener(this);
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
       this.setTitle("обработка действия");
       String txtV = txtInput.getText();
       this.setTitle("Вы ввели "+txtV);
    }
}
