
package factorialexample;


public class FactorialExample {
  //
 public static int calcFactorial(int n)
 {
     int result = 1;
     
     for(int i = 1; i <= n; i++)
     {
         result = result * i;
     }
     return result;
 }
  //  
    public static void main(String[] args) {
      
        int x = 5;
        int N = calcFactorial(x);
        int M = calcAB(2,3);
        //
        /*
        1 2 3
        1 2 3
        1 2 3
        */
        for(int A = 1; A <= 4; A++)
        {
        for(int i = 1; i <= 3; i = i + 1)
        {
            System.out.print(i);
            System.out.print(" ");
        }
        System.out.println();
        }
        //
        /*
          1 + 2! + 3! + 4!
        */   
        int sum = 0;
        for(int i = 1; i<= 4; i++)
        {
            int y = calcFactorial(i);
            sum = sum + y;
        }
        //
        sum = 0;
        for(int i = 1; i <=4; i++)
        {
            int y = 0;
            for(int j = 1; j <= i; j++)
            {
                y = y * j;
            }
            sum = sum + y;
        }
        //
        /*
        1 + 2 + 4 + 8 + 16 + 32
        0   1   2   3   4    5
        */
        sum = 0;
        for(int i = 0; i <= 5; i = i + 1)
        {
            int y = calcAB(2, i);
            sum = sum + y;
        }
        //
        /*
        3 2 1
        3 2 1
        3 2 1
        
        1
        1 2
        1 2 3
        1 2 3 4
        //вычислить сумму ряда, при помощи циклов while
        1 + 2 + 4 + 16 + 32 + 64
        */
        //
        for(int A = 1; A <= 3; A++)
        {
        for(int  i = 3; i >= 1; i = i -1)
        {
           System.out.print(i);
           System.out.print(" ");
        }
        System.out.println();
        }
        
        for(int A = 1; A <= 4; A++)
        {
            for(int i = 1; i <= A; i++)
            {
                System.out.print(i);
                System.out.print(" ");
            }
            System.out.println();
        }
         
        int i = 0;//
        while (i <= 5)
        {
            int y = 1;
            int j = 1;
            while(j <= i)
            {
                y = y * j;
                j = j + 1;
            }
            sum = sum + y;
            i = i + 1;
        }
    }
    //
    public static int calcAB(int a, int b)
    {
        int result = 1;
        for(int i = 1; i <= b; i ++)
        {
            result = result * a;
        }
        return result;
    }
    //
}
