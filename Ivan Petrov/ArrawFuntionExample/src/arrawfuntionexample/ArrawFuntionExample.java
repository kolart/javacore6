
package arrawfuntionexample;

import java.util.ArrayList;
import java.util.List;


public class ArrawFuntionExample {


    public static void main(String[] args) {
       /*
        import java.util.ArrayList;
        import java.util.List;
       */
       List<String> employees = new ArrayList<String>();
        
       employees.add("директор" );
       employees.add("бухгалтер");
       employees.add("продажник");
       
       /*
       for(String e: employees)
       {
            System.out.println(e);
       }
       */
       // employees.forEach((String e) -> {System.out.println(e);} );
       employees.forEach(e -> System.out.println(e) );
       
       
    }
    
}
